{-# LANGUAGE OverloadedStrings #-}
module Utils.Network.IO (
          getNewImages
        , rebootCams
        , rebootCam
) where

import Data.Maybe(fromJust)
import Control.Exception(
          SomeException
        , catch
        , try
        )
import Control.Applicative((<$>))
import Network(
          PortID(PortNumber)
        , connectTo
        )
import Network.HTTP.Client(
          HttpException(..)
        , Manager
        , defaultManagerSettings
        , withManager
        , httpLbs
        , responseBody
        , parseUrl
        , applyBasicAuth
        , httpNoBody
        , responseClose
        )
import Network.HTTP.Types.Status(
          Status(..)
        )
import Control.Monad.Writer(
          WriterT
        , runWriterT
        , tell
        , liftIO
        )
import System.IO(hClose)
import Crypto.Hash.MD5(hashlazy)

import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text as T

import Data
import Utils.Config

availableHost:: String -> IO Bool
availableHost ip = catch (tryPort >> return True) handler
        where
            handler :: SomeException -> IO Bool
            handler _ = return False
            tryPort = connectTo ip (PortNumber 80) >>= hClose

getNewImages :: [Cam] -> IO ([Cam], Log)
getNewImages cams = withManager defaultManagerSettings $ \m ->
       runWriterT $ do
           availableServer <- liftIO $ availableHost serverIP
           if availableServer
           then mapM (getNewImage m) cams
           else do
               tell [T.concat [
                      "Сервер "
                    , T.pack serverIP
                    , " недоступен"]
                    ]
               return []

getNewImage :: Manager -> Cam -> WriterT Log IO Cam
getNewImage manager cam = do
       respImg <- liftIO process
       case respImg of
           Right img ->
                return $ cam {newImage =
                    Just . hashlazy $ img}
           Left _ -> do
                tell [cantGet]
                return cam
       where
           process :: IO (Either HttpException BL.ByteString)
           process = try $ responseBody <$> httpLbs req manager
           req     = fromJust . parseUrl $ url
           url     = folderInServer ++ imageName cam
           cantGet = T.concat ["Изображение камеры "
                               , name cam
                               , " - "
                               , T.pack . imageName $ cam
                               , " не полученно с сервера "
                               , T.pack serverIP
                               ]

rebootCams :: [Cam] -> IO Log
rebootCams = mapM rebootCam

rebootCam :: Cam -> IO T.Text
rebootCam cam = withManager defaultManagerSettings $ \m ->
        catch (reboot m >> return logReboot) handler
        where
            reboot m  = httpNoBody req m >>= responseClose
            req       = applyBasicAuth user password . fromJust $ parseUrl url
            url       = "http://" ++ camIP cam ++ path cam
            user      = BC.pack $ login cam
            password  = BC.pack $ pass cam

            logCName  = T.concat ["Камера ", name cam, " "]
            logReboot = T.concat [logCName, "перезагруженна"]
            logFCon   = T.concat [logCName, "недоступна по адрессу ", T.pack . camIP $ cam]
            logUAuth  = T.concat [logCName, "- неправильный логин/пароль"]
            handler :: HttpException -> IO T.Text
            handler e = case e of
                NoResponseDataReceived       -> return logReboot
                FailedConnectionException{}  -> return logFCon
                FailedConnectionException2{} -> return logFCon
                StatusCodeException
                    (Status {statusCode = 401}) _ _ -> return logUAuth
                _ -> return $ T.concat
                    [logCName
                    , "- неизвестная ошибка: "
                    , T.pack . show $ e
                    ]
