module Utils.Logic (
          recastCams
        , divideCam
        , haveNewImage
) where

import Data.List(
      find
    , partition
    )
import Data.Maybe(
      isJust
    , fromMaybe
    , fromJust
    )
import qualified Data.Text as T
import Data

haveNewImage :: [Cam] -> [Cam]
haveNewImage = filter (isJust . newImage)

divideCam :: [Cam] -> ([Cam], [Cam])
divideCam = partition (\cam -> image cam == newImage cam) . haveNewImage 

recastCams :: [RawCam] -> [CamType] -> [Cam]
recastCams raw types = map (`recastCam` types) raw

recastCam :: RawCam -> [CamType] -> Cam
recastCam raw types =
    Cam
        (rawName raw)
        (rawIdCam raw)
        setPath
        (rawCamIP raw)
        setLogin
        setPass
        (fromJust $ rawImageName raw)
        (rawImage raw)
        Nothing
        
    where
        setPath    = get (const Nothing) stdPath
        setLogin   = get rawLogin stdLogin
        setPass    = get rawPass stdPass
        get f stdF =
            fromMaybe
                (case find (\x -> rawIdType raw == idCamType x) types of
                    Just a  -> stdF a
                    Nothing -> error $ concat
                        [ "Can't associate id type "
                        , show (rawIdType raw)
                        , " from id "
                        , show (rawIdCam raw)
                        , " camera \""
                        , T.unpack $ rawName raw
                        , "\" with array of types"
                        ]
                )
                (f raw)
