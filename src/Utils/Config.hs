module Utils.Config where

folderInServer :: String
folderInServer = "http://" ++ serverIP ++ "/"

serverIP :: String
serverIP = "10.74.112.233"

dbName :: String
dbName = "cam.db"

logName :: String
logName = "log.txt"
