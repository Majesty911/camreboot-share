{-# LANGUAGE OverloadedStrings #-}
module Utils.IO (
          getCameras
        , putUpdateCameras
) where

import Database.SQLite.Simple
import Control.Arrow((&&&))
import Data.Maybe(fromJust)
import Control.Exception(
          IOException
        , bracket
        , try
        )
import Control.Monad.Writer(
          execWriterT
        , runWriterT
        , liftIO
        , tell
        )

import qualified Data.ByteString as BS
import qualified Data.Text as T

import Data
import Utils.Logic(
          recastCams
        , haveNewImage
        )

getCameras :: DBName -> IO ([Cam], Log)
getCameras db = runWriterT $ do
    req <- liftIO process
    case req of
        Right (types, raw) -> return $ recastCams raw types
        Left e -> do
            tell [logFGet e]
            return []
    where
        process :: IO (Either IOException ToRecast)
        process   = try $ bracket (open db) close get
        logFGet e = T.concat [
                              "Не удалось получить данные из базы данных cam.db: "
                             , T.pack $ show e
                             ]
        getRaw    = flip query_
            "SELECT name, id_cam, ip, id_type, login, pass, name_for_image, image FROM cams"
        getTypes  = flip query_
            "SELECT id_type, type_name, std_login, std_pass, path_for_reboot FROM cam_type"
        get conn  = do
            t <- getTypes conn
            r <- getRaw conn
            return (t, r)

putUpdateCameras:: DBName -> [Cam] -> IO Log
putUpdateCameras db cams = execWriterT $ do
    ans <- liftIO process
    case ans of
        Right _ -> return ()
        Left e  -> do
            tell [logFPut e]
            return ()
    where
        process :: IO (Either IOException ())
        process = try $ bracket (open db) close putAll
        logFPut e = T.concat [
                              "Не удалось обновить данные в базе данных cam.db: "
                             , T.pack $ show e
                             ]
        put conn (img, id') = execute conn
            "UPDATE cams SET image = ? WHERE id_cam = ? " (img :: BS.ByteString, id' :: Int)
        putAll conn = mapM_ (put conn . (&&&) (fromJust . newImage) idCam) $
            haveNewImage cams
