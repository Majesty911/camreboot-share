module Data (
      DBName
    , Log
    , ToRecast
    , Cam(..)
    , RawCam(..)
    , CamType(..)
) where

import Database.SQLite.Simple.FromRow
import Control.Applicative((<*>), (<$>))

import qualified Data.ByteString as BS
import qualified Data.Text as T

type DBName   = String
type Log      = [T.Text]
type ToRecast = ([CamType], [RawCam])

data Cam = Cam { name      :: T.Text 
               , idCam     :: Int
               , path      :: String 
               , camIP     :: String
               , login     :: String
               , pass      :: String
               , imageName :: String
               , image     :: Maybe BS.ByteString
               , newImage  :: Maybe BS.ByteString
               }
                deriving Show

data RawCam = RawCam { rawName      :: T.Text
                     , rawIdCam     :: Int
                     , rawCamIP     :: String
                     , rawIdType    :: Int
                     , rawLogin     :: Maybe String
                     , rawPass      :: Maybe String
                     , rawImageName :: Maybe String
                     , rawImage     :: Maybe BS.ByteString
                     }

instance FromRow RawCam where
        fromRow = RawCam <$> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field

data CamType = CamType { idCamType :: Int
                       , typeName  :: String 
                       , stdLogin  :: String 
                       , stdPass   :: String 
                       , stdPath   :: String 
                       }


instance FromRow CamType where
        fromRow = CamType <$> field <*> field <*> field <*> field <*> field
