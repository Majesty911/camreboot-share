module Main where

import qualified Data.Text    as T
import qualified Data.Text.IO as T

import Control.Exception(
          IOException
        , handle
        )
import System.IO(
          hPutStrLn
        , stderr
        )
import Control.Concurrent.Async(
          async
        , wait
        , mapConcurrently
        )

import Utils.IO(
          getCameras
        , putUpdateCameras
        )
import Utils.Network.IO(
          getNewImages
        , rebootCam
        )
import Utils.Logic(
          divideCam
        )
import Utils.Config

main :: IO()
main = do
    (cams, logGet) <- getCameras dbName >- getNewImages
    let (toReboot, toUpdate) = divideCam cams
    asyncPut  <- async $ putUpdateCameras dbName toUpdate
    logReboot <- mapConcurrently rebootCam toReboot
    logPut    <- wait asyncPut
    handle handler $ T.appendFile logName $
        mapLog [logGet, logPut, logReboot]
    where
        handler :: IOException -> IO()
        handler e = hPutStrLn stderr $ concat [
                      "Can't append log file \""
                    , logName
                    , "\"\n\t"
                    , show e
                    ]
        mapLog    = T.unlines . concat
        (>-) fa fb = do
            (a, la) <- fa
            (b, lb) <- fb a
            return (b, la ++ lb)
